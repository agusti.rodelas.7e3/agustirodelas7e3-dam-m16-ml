import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-3, 3, 10)
linial_y = x
quadratica_y = x**2
cubica_y = x**3

a = np.array([1,2,3,4,5,6,7,8,9,10])
a *= a[::-1]
print(a)

plt.plot(x, linial_y, "-", label='Linear')
#plt.plot(x, quadratica_y, "o", label='Quadràtica')
#plt.plot(x, cubica_y, ".", label='Cúbica')
plt.plot(a,a,"o",label="Mostra")
plt.xlabel('x label')
plt.ylabel('y label')
plt.title("Simple Plot")
plt.legend()
plt.show()
