import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-2, 2, 10)

plt.plot(x, x, "-", label='linear')
plt.xlabel('x label')
plt.ylabel('y label')
plt.title("Simple Plot")
plt.legend()
plt.show()
