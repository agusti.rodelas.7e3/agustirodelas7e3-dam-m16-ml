import numpy as np
import pandas as pd
import re
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        return x

def extract_ram(x):
    match = re.search("(\d+)GB",x)
    if match:
        return int(match.groups()[0])
    else:
        return x
def extract_cpu(x):
    match = re.search("(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        return x
def extract_hdd(x):
    match = "HDD" in x
    if match:
        match = re.search("(\d+)GB\s*HDD", x)
        if match:
            return int(re.search("(\d+)GB\s*HDD", x).groups()[0])
        else:
            return int(re.search("(\d+)TB\s*HDD", x).groups()[0]) * 1024
    else:
        return 0
def extract_ssd(x):
    match = "SSD" in x
    if match:
        match = re.search("(\d+)GB\s*SSD", x)
        if match:
            return int(re.search("(\d+)GB\s*SSD", x).groups()[0])
        else:
            return int(re.search("(\d+)TB\s*SSD", x).groups()[0]) * 1024
    else:
        return 0

def extract_fs(x):
    match = "Flash Storage" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return 0

laptops = pd.read_csv("laptops.txt", encoding="utf-16")

#formatejar les dades
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["HDD"] = laptops["Memory"].apply(extract_hdd)
laptops["SSD"] = laptops["Memory"].apply(extract_ssd)
laptops["FS"] = laptops["Memory"].apply(extract_fs)

# Mètode per imprimir una gràfica amb l'error del entrenament
def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

def entrenar_polinomial(llista):
    X = laptops[llista]
    y = laptops["Price_euros"]

    X_train = X.sample(frac=0.8, random_state=2021)
    X_test = X.drop(X_train.index)

    y_train = y[X_train.index]
    y_test = y[X_test.index]

    normalizer = keras.layers.experimental.preprocessing.Normalization()
    normalizer.adapt(np.array(X_train))

    model = keras.models.Sequential()
    model.add(normalizer)

    model.add(tf.keras.layers.Dense(200, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))

    model.add(tf.keras.layers.Dense(1))

    model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.001),
                    loss="mean_squared_error",
                    metrics=tfa.metrics.RSquare(y_shape=(1,)))

    history = model.fit(X_train, y_train, epochs=100, validation_split=0.2)
    plot_loss(history)

    train_loss, train_metric = model.evaluate(X_train, y_train)
    test_loss, test_metric = model.evaluate(X_test, y_test)

    print("train loss:", train_loss, "train metric:", train_metric)
    print("test loss:", test_loss, "test metric:", test_metric)





entrenar_polinomial(["Ram", "Cpu", "ScreenResolution", "HDD", "SSD", "FS"])


