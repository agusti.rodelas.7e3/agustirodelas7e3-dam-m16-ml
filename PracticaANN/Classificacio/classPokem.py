import numpy as np
import pandas as pd
import re
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras


drugs = pd.read_csv("drug200.csv")

print(drugs.head())

drugs['Sex'] = pd.Categorical(drugs['Sex'])
drugs['Sex'] = drugs['Sex'].cat.codes

drugs['BP'] = pd.Categorical(drugs['BP'])
drugs['BP'] = drugs['BP'].cat.codes

drugs['Cholesterol'] = pd.Categorical(drugs['Cholesterol'])
drugs['Cholesterol'] = drugs['Cholesterol'].cat.codes

drugs['Drug'] = pd.Categorical(drugs['Drug'])
drugs['Drug'] = drugs['Drug'].cat.codes

print(drugs.count())

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()



def classificacio(llista):

        X = drugs[llista]
        y = drugs["Drug"]

        print(y)

        X_train = X.sample(frac=0.8, random_state=2021)
        X_test = X.drop(X_train.index)

        y_train = y[X_train.index]
        y_test = y[X_test.index]

        normalizer = keras.layers.experimental.preprocessing.Normalization()
        normalizer.adapt(np.array(X_train))

        #xarxa neuronal
        model = keras.models.Sequential()
        model.add(normalizer)

        model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dropout(0.4))
        model.add(tf.keras.layers.Dense(24, activation=tf.nn.relu))


        model.add(tf.keras.layers.Dense(len(y.unique()), activation=tf.nn.softmax))

        #omptimització
        model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.001),
                      loss="sparse_categorical_crossentropy",
                      metrics=['accuracy'])

        history = model.fit(X_train, y_train, epochs=100, validation_split=0.2)
        plot_loss(history)

        train_loss, train_metric = model.evaluate(X_train, y_train)
        test_loss, test_metric = model.evaluate(X_test, y_test)

        print("train loss:", train_loss, "train metric:", train_metric)
        print("test loss:", test_loss, "test metric:", test_metric)


classificacio(['Age', 'Sex', 'BP', 'Cholesterol', 'Na_to_K'])
