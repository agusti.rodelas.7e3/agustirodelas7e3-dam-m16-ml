import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv("Titanic.csv")
dona = data[data["Sex"] == "female"]
home = data[data["Sex"] == "male"]

age_dona = dona["Age"]
age_home = home["Age"]

llista_edats = [age_home, age_dona]

plt.hist(llista_edats, 80, label=["Male", "Famale"])
plt.xlabel("Edat")
plt.ylabel("Freqüència")
plt.legend()
plt.show()
