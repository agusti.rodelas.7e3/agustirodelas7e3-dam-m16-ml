import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from sklearn import linear_model

mtcars = pd.read_csv("mtcars.csv")

regression_model = linear_model.LinearRegression()

#Entrenar el model amb les dades de wt i mpg
regression_model.fit(X = pd.DataFrame(mtcars["wt"]),
                     y = mtcars["mpg"])

#del model entrenat mirar la intercepcio y
print(regression_model.intercept_)

print(regression_model.coef_)

regression_model.score(X = pd.DataFrame(mtcars["wt"]),
                       y = mtcars["mpg"])

train_prediction = regression_model.predict(X = pd.DataFrame(mtcars["wt"]))

residuals = mtcars["mpg"] - train_prediction

print(residuals.describe())

SSResiduals = (residuals**2).sum()

SSTotal = ((mtcars["mpg"] - mtcars["mpg"].mean())**2).sum()

mtcars.plot(kind="scatter",
            x="wt",
            y="mpg",
            figsize=(9, 9),
            color="black",
            xlim = (0,7))

plt.plot(mtcars["wt"],
         train_prediction,
         color="blue")


