import  pandas as pd

data = pd.read_csv("mushrooms.csv", encoding="utf-8", dtype="category")

data['odor'] = pd.Categorical(data['odor'])
data['class'] = pd.Categorical(data['class'])

data['odor'] = data.odor.cat.codes

print(data.head())
