import pandas as pd
import re

def extract_ram(x):
    match = re.search("(\d+)GB",x)
    if match:
        return int(match.groups()[0])
    else:
        return x


def extract_cpu(x):
    match = re.search("(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        return x


def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        return


def extract_hdd(x):
    match = "HDD" in x
    if match:
        match = re.search("(\d+)GB\s*HDD", x)
        if match:
            return int(re.search("(\d+)GB\s*HDD", x).groups()[0])
        else:
            return int(re.search("(\d+)TB\s*HDD", x).groups()[0]) * 1024
    else:
        return 0


def extract_ssd(x):
    match = "SSD" in x
    if match:
        match = re.search("(\d+)GB\s*SSD", x)
        if match:
            return int(re.search("(\d+)GB\s*SSD", x).groups()[0])
        else:
            return int(re.search("(\d+)TB\s*SSD", x).groups()[0]) * 1024
    else:
        return 0

def extract_fs(x):
    match = "Flash Storage" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return 0


laptops = pd.read_csv("laptops.txt", encoding="utf-16")

laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["HDD"] = laptops["Memory"].apply(extract_hdd)
laptops["SSD"] = laptops["Memory"].apply(extract_ssd)
laptops["FS"] = laptops["Memory"].apply(extract_fs)

train = laptops.sample(frac=0.8, random_state=2021)
test = laptops.sample(frac=0.8, random_state=2021)


