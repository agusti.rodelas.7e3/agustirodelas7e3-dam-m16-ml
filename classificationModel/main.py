import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
#Logistic Regression
from sklearn.linear_model import LogisticRegression
#Decision Tree
from sklearn.tree import DecisionTreeClassifier
#Nearest Neighbors
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix



fruits = pd.read_table('fruit_data_with_colors.txt')
print(fruits.shape)

print("type of fruits")
print(fruits['fruit_name'].unique())

print("number of fruit")
print(fruits.groupby('fruit_name').size())


print("\nTraining and Test")

feature_names = ['mass', 'width', 'height', 'color_score']
X = fruits[feature_names]
y = fruits['fruit_label']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle=True)
#X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
scaler = MinMaxScaler()

X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)


print("\nLogistic Regression")
logreg = LogisticRegression()

logreg.fit(X_train, y_train)

print("Training: {:.2f}".format(logreg.score(X_train, y_train)))
print("Test: {:.2f}".format(logreg.score(X_test, y_test)))


print("\nDecision Tree")

clf = DecisionTreeClassifier().fit(X_train, y_train)

print("Training: {:.2f}".format(clf.score(X_train, y_train)))
print("Test: {:.2f}".format(clf.score(X_test, y_test)))

print("\nK-Nearest Neighbors")

knn = KNeighborsClassifier().fit(X_train, y_train)

print("Training: {:.2f}".format(knn.score(X_train, y_train)))
print("Test: {:.2f}".format(knn.score(X_test, y_test)))

print("\nLinear Discriminant Analysis")

lda = LinearDiscriminantAnalysis().fit(X_train, y_train)

print("Training: {:.2f}".format(lda.score(X_train, y_train)))
print("Test: {:.2f}".format(lda.score(X_test, y_test)))

print("\nGaussian Naive Bayes")

gnn = GaussianNB().fit(X_train, y_train)

print("Training: {:.2f}".format(gnn.score(X_train, y_train)))
print("Test: {:.2f}".format(gnn.score(X_test, y_test)))

print("\nSupport Vector Machine")

svm = SVC()
svm = svm.fit(X_train, y_train)
print("Training: {:.2f}".format(svm.score(X_train, y_train)))
print("Test: {:.2f}".format(svm.score(X_test, y_test)))


print("Show precision knn")

pred = knn.predict(X_test)
print(confusion_matrix(y_test, pred))
print(classification_report(y_test, pred))

print("Show precision Log Regression")

pred = logreg.predict(X_test)
print(confusion_matrix(y_test, pred))
print(classification_report(y_test, pred))





