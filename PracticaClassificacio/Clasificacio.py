import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
#gràfic
import seaborn as sns
import pylab as pl

flors = pd.read_csv('iris.csv')
print("head: ")
print(flors.head)

print("shape: ")
print(flors.shape)

print("species: ")
print(flors['species'].unique())

print(flors.groupby('species').size())


plt.show()

def sephist(col):
    setosa_type = flors[flors["species"] == "setosa"][col]
    versicolor_type = flors[flors["species"] == "versicolor"][col]
    virginica_type = flors[flors["species"] == "virginica"][col]
    return setosa_type, versicolor_type, virginica_type

for num, alpha in enumerate(['sepal_length','sepal_width','petal_length', 'petal_width']):
    fig, axs = plt.subplot(3)
    axs[0,num].hist((sephist(alpha)[0], sephist(alpha)[1], sephist(alpha)[2]), bins=25, alpha=0.5, label=['setosa', 'versicolor', 'virginica'], color=['r', 'b', 'y'])
    axs[0,num].set_title(alpha)

plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.show()

