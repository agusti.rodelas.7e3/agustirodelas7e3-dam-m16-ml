def girar_str(str):
    contingut = ""
    for x in reversed(str):
        contingut += x.lower()

    return contingut

def comprobar_palindrom(paraula):
    #if(paraula == paraula[::-1]) amb [::-1] dones la volta
    if(girar_str(paraula) == paraula.lower()):
        print("És palíndrom")
    else:
        print("No és palíndrom")


paraula = input("introdueix una frase: ")
comprobar_palindrom(paraula)


