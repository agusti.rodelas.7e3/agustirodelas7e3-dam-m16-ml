def generar_triangle(n):
    triangle = [[1],[1,1]]
    for i in range(n-2):

        linia=[1]
        pos_linia = len(triangle) - 1
        index_linia = 0
        suma_index_linia = index_linia
        for j in range(len(triangle)-1):

            suma_index_linia = suma_index_linia + 1
            linia.append(triangle[pos_linia][index_linia]+triangle[pos_linia][suma_index_linia])
            index_linia = index_linia+1

        linia.append(1)
        triangle.append(linia)

    return triangle

def dibuxar_tringle(tringle):
    espai_num = 0
    for i in range(len(tringle)):
        print(" " * (len(tringle)-espai_num), end=" ")
        for j in range(len(tringle[i])):
            print(tringle[i][j], end=" ")
        print("")
        espai_num += 1


num = int(input("Introdueix un enter: "))
dibuxar_tringle(generar_triangle(num))