import numpy as np
import pandas as pd
import re
import matplotlib
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split

def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        return x

def extract_ram(x):
    match = re.search("(\d+)GB",x)
    if match:
        return int(match.groups()[0])
    else:
        return x
def extract_cpu(x):
    match = re.search("(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        return x
def extract_hdd(x):
    match = "HDD" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return 0

def extract_ssd(x):
    match = "SSD" in x
    if match:
        match = "HDD" in x
        if match:
            return extract_hdd_sdd(x)
        else:
            match = "GB" in x
            if match:
                return int(re.search("(\d+)[G, T]B", x).groups()[0])
            else:
                return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return extract_hdd(x)

def extract_fs(x):
    match = "Flash Storage" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return 0

def extract_hdd_sdd(x):
    hdd_str = str(re.search("\d+[GT]B\b+HDD", x))
    ssd_str = str(re.search("\d+[GT]B\b+SSD", x))

    match = "TB" in hdd_str
    if match:
        hdd = int(re.search("(\d+)[GT]B", hdd_str).groups()[0]) * 1024
    else:
        hdd = int(re.search("(\d+)[GT]B", hdd_str).groups()[0])

    match = "TB" in ssd_str
    if match:
        ssd = int(re.search("(\d+)[GT]B", ssd_str).groups()[0]) * 1024
    else:
        ssd = int(re.search("(\d+)[GT]B", ssd_str).groups()[0])

    return ssd + hdd

laptops = pd.read_csv("laptops.txt", encoding="utf-16")

#formatejar les dades
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["HDD"] = laptops["Memory"].apply(extract_hdd)
laptops["SSD"] = laptops["Memory"].apply(extract_ssd)
laptops["FS"] = laptops["Memory"].apply(extract_fs)

print(laptops)

'''#Colocar les dades en X i y
y = laptops["Price_euros"]
X = laptops.loc[:,["Ram", "Cpu"]]

#seprar les dades en Entrenament i en test (prporció 80-20%)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle=True)

regression_model = linear_model.LinearRegression()

regression_model.fit(X_train, y_train)

print(regression_model.intercept_)

print(regression_model.coef_)

train_prediction = regression_model.predict(X = laptops.loc[:, ["Ram","Cpu"]])

print(regression_model.score(X_test,
                       y_test))

laptops.plot(kind="scatter",
             x="Ram",
             y="Price_euros",
             figsize=(9,9),
             color="black",
             xlim=(0,7))

plt.plot(laptops.loc[:, ["Ram","Cpu"]],
         train_prediction,
         color="blue")'''

