import numpy as np
import pandas as pd
import re
import matplotlib
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split

def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        return x

def extract_ram(x):
    match = re.search("(\d+)GB",x)
    if match:
        return int(match.groups()[0])
    else:
        return x
def extract_cpu(x):
    match = re.search("(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        return x
def extract_hdd(x):
    match = "HDD" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B\b*HDD", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B\b*HDD", x).groups()[0]) * 1024
    else:
        return 0
def extract_ssd(x):
    match = "SSD" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[GT]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[GT]B", x).groups()[0]) * 1024
    else:
        return 0
def extract_fs(x):
    match = "Flash Storage" in x
    if match:
        match = "GB" in x
        if match:
            return int(re.search("(\d+)[G, T]B", x).groups()[0])
        else:
            return int(re.search("(\d+)[G, T]B", x).groups()[0]) * 1024
    else:
        return 0

laptops = pd.read_csv("laptops.txt", encoding="utf-16")

#formatejar les dades
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["HDD"] = laptops["Memory"].apply(extract_hdd)
laptops["SSD"] = laptops["Memory"].apply(extract_ssd)
laptops["FS"] = laptops["Memory"].apply(extract_fs)

def entranar_linial(llista):
    X = laptops[llista]
    y = laptops["Price_euros"]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle=True)

    regression_model = linear_model.LinearRegression()

    regression_model.fit(X_train, y_train)

    print(regression_model.score(X_test,
                       y_test))


#Colocar les dades en X i y
y = laptops["Price_euros"]
X = pd.DataFrame(laptops["Ram"])

print("Ram: ")
entranar_linial(["Ram"])
print("Ram i Cpu: ")
entranar_linial(["Ram", "Cpu"])
print("Ram, Cpu i Screen Resolution: ")
entranar_linial(["Ram", "Cpu", "ScreenResolution"])














